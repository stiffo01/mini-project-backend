FROM python:3
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
ENTRYPOINT ["gunicorn", "src.backend:app", "--worker-class", "uvicorn.workers.UvicornWorker", "--bind", "0.0.0.0:80"]
